﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laba1_3S
{
    public partial class MyMainForm : Form
    {
        public MyMainForm()
        {
            InitializeComponent();
        }

        private void Task(object sender, EventArgs e)
        {
            if (textBox1.Text.Length == 0)
            {
                MessageBox.Show("Не введена фамилия");
                return;
            }
            if (textBox2.Text.Length == 0)
            {
                MessageBox.Show("Не введено имя");
                return;
            }
            if (textBox3.Text.Length == 0)
            {
                MessageBox.Show("Не введено отчество");
                return;
            }
            if (comboBox1.Text.Length == 0)
            {
                MessageBox.Show("Не введен класс");
                return;
            }
            if (Prosto.Checked == false && Slojno.Checked == false)
            {
                MessageBox.Show("Не выбран тип сложности задания");
                return;
            }
            if (numberofquestions.Value == 0)
            {
                MessageBox.Show("Не введено количество вопросов");
                return;
            }

            TaskForm tf = new TaskForm(textBox1.Text, comboBox1.Text, numberofquestions.Value,  Slojno.Checked); 
            tf.ShowDialog();
        

        }

    }
}
