﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laba1_3S
{

    public partial class TaskForm : Form
    {
        Random rand;

        public TaskForm(string name, string group, decimal numberofquestions, bool hard)
        {
            InitializeComponent();

            this.rand = new Random();
            information.Text = "Ученик: " + name + "\n" + "Класс: " + group;
            int n = Convert.ToInt32(numberofquestions);
            string[] hardlist = new string[] { "Cколько будет А + В? ", "Сколько у тебя денег? ", "Какая завтра температура? ", "У нее номер 893725271..?" };

            for (int i = 1; i <= n; i++)
            {
                Label questions = new Label();
                if (hard)
                {

                    questions.Text = i + ") " + hardlist[rand.Next(4)];
                    questions.Size = new Size(questions.PreferredWidth, questions.PreferredHeight);
                }
                else
                {
                    questions.Text = "Вопрос №" + i;
                    questions.Size = new Size(questions.PreferredWidth, questions.PreferredHeight);
                }

                int x, y;
                x = 20;
                y = 20 + (i * 50);
                questions.Location = new Point(x, y);
                this.Controls.Add(questions);
                if (hard)
                {
                    ComboBox hardanswers = new ComboBox();
                    for (int j = 0; j < n; j++) hardanswers.SelectedIndex = rand.Next(hardanswers.Items.Add(rand.Next(5, 50)));
                    hardanswers.AutoSize = true;
                    hardanswers.Location = new Point(50, y + 20);
                    this.Controls.Add(hardanswers);
                }
                else
                {
                    for (int j = 1; j <= 4; j++)
                    {

                        CheckBox easyanswers = new CheckBox();
                        easyanswers.Text = j.ToString();
                        easyanswers.AutoSize = true;
                        easyanswers.Location = new Point(j * 50, y + 20);
                        this.Controls.Add(easyanswers);
                    }
                }
            }
        }
        private void Exit(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
